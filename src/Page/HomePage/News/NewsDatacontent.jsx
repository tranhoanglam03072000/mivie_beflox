export const contentDa24h = [
  {
    id: 1,
    hinhAnh:
      "https://scontent.fsgn2-7.fna.fbcdn.net/v/t39.30808-6/311565230_472474104915319_4399149922976434110_n.jpg?_nc_cat=100&ccb=1-7&_nc_sid=730e14&_nc_ohc=uNCokOdFztAAX_-WDKU&_nc_ht=scontent.fsgn2-7.fna&oh=00_AfAI3pTAFpWo3LqTc-kBaDXUpCYSqgduKMzELHBQzEmYcw&oe=6384FEED",
    content:
      "Chiều thứ 7 và chủ nhật hằng tuần vào 14 giờ chiều theo giờ Việt Nam, động lươn BC33E Frontend do thầy Sĩ lãnh đạo đưa ra bí quyết chính thức về việc code sao cho không mất lòng tester và designer giúp cuộc sống công ty hạnh phúc...",
    title: "Thầy Sĩ pro chỉ phương pháp...",
  },
  {
    id: 2,
    hinhAnh:
      "https://s3img.vcdn.vn/123phim/2020/07/khi-phu-nu-khong-con-o-the-tron-chay-cua-nan-nhan-15943683481617.jpg",
    content:
      "Là bộ phim tâm lý li kỳ với chủ đề tội phạm, Bằng Chứng Vô Hình mang đến một góc nhìn mới về hình ảnh những người phụ nữ thời hiện đại. Điều đó được thể hiện qua câu chuyện về hai người phụ nữ cùng hợp sức để vạch mặt tên tội phạm có sở thích bệnh hoạn với phụ nữ.",
    title: "Khi phụ nữ không còn ở thế trốn chạy của nạn nhân",
  },
  {
    id: 3,
    hinhAnh:
      "https://s3img.vcdn.vn/123phim/2020/07/gerard-butler-cung-bo-cu-deadpool-tham-gia-greenland-15937528932506.png",
    content:
      "Bộ phim hành động mang đề tài tận thế Greenland: Thảm Họa Thiên Thạch đến từ nhà sản xuất của loạt phim John Wick đã tung ra trailer đầu tiên, hé lộ nội dung cốt truyện, dàn",
    title: "Gerard Butler cùng bồ cũ Deadpool tham gia Greenland",
  },
  {
    id: 4,
    hinhAnh:
      "https://s3img.vcdn.vn/123phim/2020/07/dien-vien-dac-biet-cua-bang-chung-vo-hinh-15937518743844.png",
    content:
      "Bằng Chứng Vô Hình tiết lộ thêm với khán giả một diễn viên vô cùng đặc biệt, đi diễn như đi chơi và không hề nghe theo sự chỉ đạo của đạo",
    title: "Diễn viên đặc biệt của Bằng Chứng Vô Hình ",
  },
  {
    id: 5,
    hinhAnh:
      "https://s3img.vcdn.vn/123phim/2020/07/pee-nak-2-van-kiep-thien-thu-di-tu-khong-het-nghiep-15937498464029.png",
    content: "",
    title: "Pee Nak 2 - Vạn kiếp thiên thu, đi tu không hết nghiệp!",
  },
  {
    id: 6,
    hinhAnh:
      "https://s3img.vcdn.vn/123phim/2020/07/loat-phim-kinh-di-khong-the-bo-lo-trong-thang-7-15937470779379.png",
    content: "",
    title: "Loạt phim kinh dị không thể bỏ lỡ trong tháng 7!",
  },
  {
    id: 7,
    hinhAnh:
      "https://s3img.vcdn.vn/123phim/2020/06/rom-tung-trailer-he-lo-cuoc-song-cua-dan-choi-so-de-15929959532579.jpg",
    content: "",
    title: "RÒM tung trailer hé lộ cuộc sống của dân chơi số đề",
  },
  {
    id: 8,
    hinhAnh:
      "https://s3img.vcdn.vn/123phim/2020/06/antebellum-trailer-cuoi-cung-khong-he-lo-bat-cu-thong-tin-gi-them-15929866818923.jpg",
    content: "",
    title: "Antebellum - Trailer cuối cùng không hé lộ bất cứ thông tin gì",
  },
];
